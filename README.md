Installation 

* Create config.json file 

{
	"host" : "localhost",
	"user" : \<username\>,
	"password" : \<password\>,
	"database" : "Certif",
	"port" : "8000"
}


* Create BDD : 
    1. import scriptBDD.sql 
    2. import BDDdata.sql

* nodeJS :
 1. cd nodeJs
 2. npm install
 3. node api.js
 
* Front: 
 1. cd front
 2. npm install
 3. npm run serve
 
 -> go to localhost:8080