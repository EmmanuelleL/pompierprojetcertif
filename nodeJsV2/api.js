var express = require('express');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer');
const Sequelize = require('sequelize');
var cors = require('cors');
var UserCtrl = require('./controlleur/UserCtrl');
var apiRouter = require('./routes/apiRouter').router;


var config = require('./config/config.json').development

var models = require('./models')

var app = express();

const sequelize = new Sequelize(config.database, config.username, config.password, {
	host: 'localhost',
	dialect: 'mysql',
})

sequelize
	.authenticate()
	.then(() => {
		console.log('Connection has been established successfully.');
	})
	.catch(err => {
		console.error('Unable to connect to the database:', err);
	});

passport.use(new BearerStrategy(
	function(token, done) {
	console.log('autentification..')
		models.Users.findOne({where :{token: token }}).then(function (user) {
			
			if (!user) { return done(null, false); }
			return done(null, user, { scope: 'all' });
		})
		.catch(err=>{
			 return done(err); 
		});
	}
));

app.use(cors({origin: '*'}))


 app.use('/',apiRouter);



app.listen(config.apiport,	()=>{
	console.log("server started at port "+ config.apiport);
	});