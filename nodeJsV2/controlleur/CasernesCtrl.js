var models = require('../models')

module.exports = {
	getAll : function (req, res) {
		models.Users.findByPk(req.user.id, {include : [{model : models.Profils, required: true}]})
		.then((user)=>{
			if(user.Profil.nom == 'SuperAdmin'){
				models.Casernes.findAll()
				.then((casernes)=>{
					res.send(casernes)
				})
				.catch((err)=>{
					return res.send([])
				})
			}else if(user.Profil.nom == 'Responsable Caserne'){
				models.Casernes.findAll({where : {Responsable_id : req.user.id}})
				.then((casernes)=>{
					res.send(casernes)
				})
				.catch((err)=>{
					return res.send([])
				})

			}else if(user.Profil.nom == 'Responsable Equipe'){
				models.Casernes.findAll({include : [{
												model : models.Equipes, 
												where : {Responsables_id : req.user.id},
												required: true,
												}]})
				.then((casernes)=>{
					res.send(casernes)
				})
				.catch((err)=>{
					return res.send([])
				})
			}else{
				console.log(user.Profil.nom)
				return res.send([])
			}
		})
		
	},

	getCasernesUser : function(req, res){
		console.log('select user\'s casernes')
		models.Casernes.findAll({ attributes : ['id','nom', 'adresse'],
								  include : [{
											model : models.Equipes,
											attributes : ['id'], 
											required: true,
											include : [{
												model : models.Users,
												as : 'Equipe',
												attributes : ['id'],
												where : {id : req.user.id},
												required: true,
											}]
										}]})
		.then((casernes)=>{
					res.send(casernes)
				})
				.catch((err)=>{
					console.log(err)
					return res.send(err)
				})
	},

	getUsersCaserne : function(req, res){
		models.Casernes.findByPk(req.params.id,{ attributes : ['id'],
								  include : [{
											model : models.Equipes,
											attributes : ['id'], 
											required: true,
											include : [{
												model : models.Users,
												as : 'Equipe',
												attributes : ['id', 'nom', 'email', 'Profils_id', 'Grades_id', 'prenom'],
												required: true,
												include : [{model : models.Profils, required: true}]
											}]
										}]})
		.then((casernes)=>{
			var Users = [];
			for (var i = 0; i < casernes.Equipes.length; i++) {
				Users =Users.concat(casernes.Equipes[i].Equipe)
			}

					res.send(Users)
				})
				.catch((err)=>{
					return res.send(err)
				})
	},

	save : function(req, res){
		models.Casernes.create({
			nom : req.query.nom,
			adresse: req.query.adresse,
			Responsables_id : req.query.responsable
		})	
		.then(newCaserne => {
			newCaserne.save().then((savedCaserne)=>{ 
					return res.send(savedCaserne)
				}).catch((errr)=>{
					console.log(err)
				})
		})
		.catch((err)=>{
			console.log(err)
		})
	},

	delete : function(req, res){
		models.Casernes.findByPk(req.params.id, {include : [{model : models.Equipes}]})
		.then((caserne)=>{
			if (caserne.Equipes.length){
				return res.send('supprimer d\'abord les équipes de cette caserne')
			}else{
				caserne.destroy().then(()=>{
					return res.send('caserne supprimé')
				})
			}
		})
	}
}