var User = require('../models').Users;
var bcryptjs = require('bcryptjs');

var models = require('../models')


var saltRounds = 10;

hashMpd = function(mdp){
		return new Promise((resolve, reject)=>{
			bcryptjs.hash(mdp, saltRounds, function(err, hash) {
				resolve(hash);
			});
		})
	};
generateToken = function(){		return new Promise((resolve, reject)=>{
			var text = "";
				var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

				for (var i = 0; i < 60; i++){
					text += possible.charAt(Math.floor(Math.random() * possible.length));
				}

				resolve(text);
			})
	};

const EMAIL_REGEX  = /^[a-zA-Z0-9_\-.]+@[a-zA-Z\-_]+\.[a-z]{2,4}$/;


module.exports = {

	save: function (req, res) {


		if (req.query.nom == null || req.query.prenom == null || req.query.mdp == null || req.query.email == null || req.query.Grades_id == null || req.query.Profils_id == null  ) {
			      return res.status(400).send('error missing parameters');
			}
			
		 if (!EMAIL_REGEX.test(req.query.email)) {
		    return res.status(400).send('error email is not valid');
		}

		Promise.all([hashMpd(req.query.mdp),User.findOne({where : {email : req.query.email}})])
		.then(result=>{

			if(result[2]){
				return res.status(409).send('email deja utilisé')
			}

			var mdp = result[0];

			User.create({
				nom: req.query.nom,
				prenom: req.query.prenom,
				email : req.query.email,
				avatar : null,
				mdp : mdp,
				Profils_id : req.query.Profils_id,
				Grades_id : req.query.Grades_id,
				CreatedAt: new Date(),
	    		UpdatedAt: new Date()
			}).then(newUser => {
				newUser.save().then((savedUser)=>{ 
						return res.send(savedUser)
					})
			})
		})


	},

	login : function(req, res){

			User.findOne({where : {email : req.query.email}}).then((user)=>{
				if(!user){
					return res.send("erreur")
				}
				bcryptjs.compare(req.query.mdp, user.mdp, function(err, result) {
				    if(result == true){
				    	generateToken().then((token)=>{
				    		user.update({ token: token}).then(() => {
							 return res.send(token)
							})
						})
				    }else{
				    	return res.send("erreur")
				    }
				});
			})
	},

	logout : function (req, res){
		req.user.update({ token: null})
		.then(() => {
					return res.send('déconecté')
				})
	},

	getbyid : function(req, res){
		User.findByPk(req.params.id, {include : [{
			model : models.Grades,
		},{
			model: models.Profils,
		},{
			model: models.Equipes,
			as : 'Equipe',
			include : [{
				model: models.Casernes,
			}],
		}]}).then((user)=>res.send(user.get()))
	},

	getUser : function(req, res){
		User.findByPk(req.user.id, {attibute : ['nom', 'prenom', 'avatar', 'CreatedAt', 'UpdatedAt', 'email', 'Profils_id', 'Grades_id'],
			include : [{
				model : models.Grades
			}]}).then((user)=>res.send(user.get()))
	},

	getEquipe : function(req, res){

		var user = User.findByPk(req.params.id, { include :  [{
												as : 'Equipe',
												model: models.Equipes,
												attributes : ['numero', 'Responsables_id'],
												where : { Casernes_id :req.query.Casernes_id}
												}]})
		.then((user)=>{
			return res.send(user.Equipe)
		})
		.catch((err)=>{
			return res.send([])
				})

	},

	getAll : function(req,res){
		 User.findAll({include : [{
			model : models.Grades,
		},{
			model: models.Profils,
		}]}).then((all)=>{
			return res.send(all)
		})
		
	},

	getAllProfils : function(req, res){
		models.Profils.findAll().then((all)=>{
			return res.send(all)
		})
	},

	getAllGrades : function(req, res){
		models.Grades.findAll().then((all)=>{
			return res.send(all)
		})
	},

	getAllAptitudes : function(req, res){
		models.Aptitudes.findAll().then((all)=>{
			return res.send(all)
		})
	},

	getUserAptitudes :  function(req, res){

		var user = User.findByPk(req.params.id, { include :  [{
												model: models.Aptitudes,
												}]})
		.then((user)=>{
			return res.send(user.Aptitudes)
		})
		.catch((err)=>{
			return res.send([])
				})
	},

	addAptitudes: function(req, res){
		models.Users_has_Aptitudes.create({
			Users_id : req.query.Users_id,
			Aptitudes_id : req.query.Aptitudes_id,
		}).then(()=>{
			return res.send("Aptitude ajoutée")
		})
		.catch((err)=>{
			return res.send([])
		})
	},

	deleteAptitudes: function(req, res){
		models.Users_has_Aptitudes.findOne({where : {Users_id : req.query.Users_id, Aptitudes_id : req.query.Aptitudes_id}})
		.then((find)=>{
			find.destroy().then(()=>{
				return res.send("Aptitude supprimée")
			}).catch((err)=>{
			return res.send([])
				})
		}).catch((err)=>{
			return res.send([])
				})
	},
	update : function(req, res){
		console.log(req.query)
		
		models.Users.findByPk(req.params.id).then((user)=>{
			if(req.query.mdp){
				hashMpd(req.query.mdp).then((mdp)=>{
					req.query.mdp = mdp;
					user.update(req.query).then((user)=>{
						console.log(user.nom)
						return res.send(user)
					}).catch((err)=>{
						console.log(err)
						return res.send([])
					})
				})
			}else{
				user.update(req.query).then((user)=>{
					console.log(user.nom)
					return res.send(user)
				}).catch((err)=>{
					console.log(err)
					return res.send([])
				})
			}
		})
	},

	delete : function(req, res){
		console.log('select user befor delete');
		User.findByPk(req.params.id, {include :[
									{model : models.Equipes,as :'Equipe', },
									{ model : models.Equipes,as :'Responsable_Equipe',
									include:[{model: models.Casernes}],
								},
									{model : models.Casernes, as :'Responsable_Caserne'},
									{model : models.Aptitudes}]})
			.then((user)=>{

				if(user.Responsable_Equipe.length){
					
					return res.send(user.nom+' ' +user.prenom+' est Responsable de l\'equipe '+user.Responsable_Equipe[0].numero +' de la caserne '+user.Responsable_Equipe[0].Caserne.nom+ '. Supprimer d\'abord cette equipe')
				}

				if(user.Responsable_Caserne.length){
					return res.send(user.nom+' ' +user.prenom+' est Responsable de la caserne '+user.Responsable_Caserne[0].nom +'. Supprimer d\'abord cette caserne')
				}

				var legnthE = "non defini";
				var legnthA ="non defini";
				var legnthP = "non defini";

				if(user.Equipe.length){
					legnthE =user.Equipe.length;
					var i=0;
					while(legnthE > 0){
						user.Equipe[i].Users_has_Equipes.destroy().catch((err)=>{
							console.log(err)
							return res.send(err)		
						})
						i++;
						legnthE --;
						if(legnthP === 0 && legnthE ===0 && legnthA ===0){
							user.destroy().then(()=>{
								return res.send("Utilisateur supprimé")
							}).catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
						}
					}
					
				}else{
					legnthE = 0
					if(legnthP === 0 && legnthE ===0 && legnthA ===0){
							user.destroy().then(()=>{
								return res.send("Utilisateur supprimé")
							}).catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
						}
				}

				if(user.Aptitudes.length){
					legnthA =user.Aptitudes.length;
					var j=0;
					while(legnthA > 0){
						user.Aptitudes[j].Users_has_Aptitudes.destroy().catch((err)=>{
							console.log(err)
							return res.send(err)		
						})
						j++;
						legnthA --;
						if(legnthP === 0 && legnthE ===0 && legnthA ===0){
							user.destroy().then(()=>{
								return res.send("Utilisateur supprimé")
							}).catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
						}
					}
					
				}else{
					legnthA = 0
					if(legnthP === 0 && legnthE ===0 && legnthA ===0){
							user.destroy().then(()=>{
								return res.send("Utilisateur supprimé")
							}).catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
						}
				}

				
				models.Posts.findAll({where : {Users_id : user.id}}).then((posts)=>{
					console.log(posts)
					if(posts.length){
						legnthP =posts.length;
						var k=0;

							console.log(legnthP + ' '+k )
						while(legnthP > 0){
							posts[k].destroy().catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
							k++;
							legnthP --;
							console.log(legnthP + ' '+k )
							if(legnthP === 0 && legnthE ===0 && legnthA ===0){
								user.destroy().then(()=>{
									return res.send("Utilisateur supprimé")
								}).catch((err)=>{
									console.log(err)
									return res.send(err)		
								})
							}
						}

					}else{
						legnthP = 0
						if(legnthP === 0 && legnthE ===0 && legnthA ===0){
							user.destroy().then(()=>{
								return res.send("Utilisateur supprimé")
							}).catch((err)=>{
								console.log(err)
								return res.send(err)		
							})
						}
					}
					
				});
			
		}).catch((err)=>{
			console.log(err)
			return res.send(err)
		})
	},
}