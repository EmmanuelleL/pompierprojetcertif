var models = require('../models')

module.exports = {
	getAll: function(req,res){
		models.Notifications.findAll({where : {Users_id :req.user.id }}).then((notifs)=>{
			return res.send(notifs);
		}).catch((err)=>{
			return res.send(err)
		})
	},

	save : function(req,res){
		if(req.query.type == "commentaire"){
		message_id = 1
		}

		if(req.query.type == "reponse"){
			message_id = 2
		}
		models.Notifications.create({
				Messages_id: message_id,
				Posts_id: req.params.id,
				Users_id: req.query.Users_id,
				CreatedAt: new Date(),
	    		UpdatedAt: new Date()
			}).then(newNotification => {
				newNotification.save().then((savedNotification)=>{ 
						return res.send(savedNotification)
					}).catch((err)=>{
						console.log(err)
						return res.send(err)
					})
			}).catch((err)=>{
				console.log(err)
			return res.send(err)
		})
	},

	getPostNotifs : function(req, res){
		models.Notifications.findAll({where : {Posts_id :req.params.id }})
		.then((notifs)=>{
			return res.send(notifs)
		}).catch((err)=>{
			return res.send(err)
		})
	},

		delete : function(req,res){
		models.Notifications.findOne({where : {Posts_id : req.params.id , Users_id : req.user.id}})
		.then((find)=>{
			find.destroy().then(()=>{
				return res.send("Notifications supprimé")
			}).catch((err)=>{
			return res.send(err)
				})
		}).catch((err)=>{
			return res.send(err)
				})
	},
}