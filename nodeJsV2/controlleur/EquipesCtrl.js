var models = require('../models')

module.exports = {
	getAll : function (req,res) {
			models.Users.findByPk(req.user.id, {include : [{model : models.Profils}]})
			.then((user)=>{
				if(user.Profil.nom == 'SuperAdmin'){
					models.Equipes.findAll({where : {Casernes_id : req.query.Casernes_id}})
					.then((equipe)=>{
						res.send(equipe);
					}).catch((err)=>{
						res.send(err)
					})
				}else if(user.Profil.nom == 'Responsable Caserne'){
					models.Casernes.findByPk(req.query.Casernes_id)
					.then((casernes)=>{
						if(casernes.Responsables_id != req.user.id){
							res.send([])
						}else{
							models.Equipes.findAll({where : {Casernes_id : req.query.Casernes_id}})
							.then((equipe)=>{
								res.send(equipe);
							}).catch((err)=>{
								res.send(err)
							})
						}
					}).catch((err)=>{
						res.send(err)
					})
				}else if(user.Profil.nom == 'Responsable Equipe'){
					models.Equipes.findAll({where : {
						Casernes_id : req.query.Casernes_id, 
						Responsables_id : req.user.id
					}})
					.then((equipe)=>{
						res.send(equipe);
					}).catch((err)=>{
						res.send(err)
					})
			}else{
				res.send([])
			}
		})
	},

	getbyid : function(req, res){
		models.Equipes.findByPk(req.params.id)
		.then((equipe)=>{
			res.send(equipe);
		}).catch((err)=>{
			res.send(err)
		})
	},

	getUsers: function(req,res){
		models.Users.findAll({include : [{
			model : models.Equipes,
			as : 'Equipe',
			where : {id : req.params.id}
		}]})
		.then((users)=>{
			res.send(users);
		}).catch((err)=>{
			res.send(err)
		})
	},

	save : function(req, res){
		if(req.query.responsable=== "NULL" || req.query.responsable === "undefined" || typeof(req.query.responsable)==="undefined"){
			req.query.responsable = null;
		}
		models.Equipes.create({
			numero : req.query.numero,
			Responsables_id : req.query.responsable,
			Casernes_id : req.query.Casernes_id
		}).then((newEquipe)=>{
			newEquipe.save().then((savedEquipe)=>{
				models.Users_has_Equipes.create({
				Users_id : req.query.responsable,
				Equipes_id : savedEquipe.id,
				})
				.then((association)=>{
					association.save()
					.catch((err)=>{
					res.send(err)
				})
				}).catch((err)=>{
					res.send(err)
				})
				return res.send(savedEquipe)
			}).catch((err)=>{
				console.log("err save "+ err)
				res.send(err)
			})
		}).catch((err)=>{
			console.log("err create " +err)
			res.send(err)
		})
	},

	update : function(req,res){
		models.Equipes.findByPk(req.params.id).then((equipe)=>{
			equipe.update({
				numero : req.query.numero,
				Responsables_id : req.query.responsable,
				Casernes_id : req.query.Casernes_id
			}).then((equipe)=>{
				res.send(equipe)
			}).catch((err)=>{
				res.send(err)
			})
		})
	},

	addUser : function(req,res){
		models.Users_has_Equipes.create({
			Users_id : req.query.Users_id,
			Equipes_id : req.query.Equipes_id
		}).then((association)=>{
			association.save().then((association)=>{
				res.send({success : "User "+req.query.Users_id +" ajouter à l'équipe "+req.query.Equipes_id})
			}).catch((err)=>{
				res.send(err)
			})
		}).catch((err)=>{
			res.send(err)
		})
	},

	delete : function(req, res){
		models.Equipes.findAll({where : {id : req.params.id,}, include : [{model : models.Users,
																as : 'Equipe',}]})
		.then((equipe)=>{
			console.log(equipe)
			for (var i = 0; i < equipe[0].Equipe.length; i++) {
					equipe[0].Equipe[i].Users_has_Equipes.destroy()
				}
			equipe[0].destroy().then(()=>{
				
				return res.send('equipe supprimé')
			})
		})
	}
}