var models = require('../models')

module.exports = {
	getAll : function (req,res) {
		models.Posts.findAll({where : {Casernes_id : req.query.Casernes_id},
								include : [{
									as : 'Sujets',
									model: models.Sujets,
								},{
									model : models.Users,
									include : [
										{model: models.Grades}
									]
								}]})
		.then((posts)=>{
			if(!posts){
				return res.send([])
			}
			return res.send(posts)
		})
		.catch((err)=>{
			return res.send(err)
		})
	},

	getUserPosts: function (req,res) {
		models.Posts.findAll({where : {Users_id : req.params.id},
												include : [{
											as : 'Sujets',
											model: models.Sujets,
										}]})
		.then((posts)=>{
			if(!posts){
				return res.send([])
			}
			return res.send(posts)
		})
		.catch((err)=>{
			return res.send(err)
		})
	},

	getDetailPosts : function (req,res) {
		models.Posts.findAll({
			include : [{
				model : models.Commentaires ,
				include : [{
					model: models.Users
				}]
			},{
				model : models.Posts_has_Response,
				include : [{
					model: models.Users
				}]
			},{
				as : 'Sujets',
				model: models.Sujets,
			},{
				model : models.Users,
				include : [
					{model: models.Grades}
				]
			}],
			where : {id :req.params.id},})
		.then((posts)=>{
			if(!posts){
				return res.send([])
			}
			return res.send(posts)
		})
		.catch((err)=>{
			return res.send(err)
		})
	},
	save : function(req,res){
		models.Posts.create({
				horaire_debut: req.query.horaire_debut,
				horaire_fin: req.query.horaire_fin,
				Users_id: req.query.Users_id,
				Sujets_id:  req.query.sujet_id,
				Casernes_id: req.query.Casernes_id,
				CreatedAt: new Date(),
	    		UpdatedAt: new Date()
			}).then(newPost => {
				newPost.save().then((savedPost)=>{ 
						return res.send(savedPost)
					}).catch((err)=>{
				return res.send(err)
			})
			}).catch((err)=>{
			return res.send(err)
		})
	},

	update : function(req,res){
		models.Posts.findByPk(req.params.id, {where : {Users_id : req.user.id}}).then((posts)=>{
			posts.update({
					horaire_debut: req.query.horaire_debut,
					horaire_fin: req.query.horaire_fin,
		    		UpdatedAt: new Date()
				}).then(newPost => {
							return res.send(newPost)
				}).catch((err)=>{
					console.log(err)
					return res.send(err)
			})
		})
	},

	delete : function(req,res){
		models.Posts.findOne({where : {id : req.params.id , Users_id : req.user.id}})
		.then((find)=>{
			find.destroy().then(()=>{
				return res.send("Post supprimé")
			}).catch((err)=>{
			return res.send(err)
				})
		}).catch((err)=>{
			return res.send(err)
				})
	},

	repondre : function(req,res){
		models.Posts_has_Response.create({
			Users_id : req.user.id,
			Posts_id : req.params.id,
			horaire_debut: req.query.horaire_debut,
			horaire_fin: req.query.horaire_fin,
		}).then((reponse)=>{
			reponse.save().then((reponse)=>{
				res.send('reponse envoyée')
			}).catch((err)=>{
				res.send(err)
			})
		}).catch((err)=>{
			res.send(err)
		})
		
	},
}