'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert('Grades', [
				{
					nom: 'Lieutenant',
					short :'ltn',
					grade : 1
				},
				{
					nom: 'Adjudant-chef',
					short :null,
					grade : 2
				},
				 {
				 	nom: 'Adjudant',
					short :null,
					grade : 3
				},
				{
					nom: 'sergent-chef',
					short :'sgt-chef',
					grade : 4
				},
				{
					nom: 'sergent',
					short :'srg',
					grade : 5
				},
				{
					nom: 'caporal-chef',
					short :null,
					grade : 6
				},
				{
					nom: 'caporal',
					short :null,
					grade : 7
				},
				{
					nom: 'première Classe',
					short :null,
					grade : 8
				},
				{
					nom: 'seconde Classe',
					short :null,
					grade : 9
				},
			], {});
		/*
			Add altering commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkInsert('People', [{
				name: 'John Doe',
				isBetaMember: false
			}], {});
		*/
	},

	down: (queryInterface, Sequelize) => {

		return queryInterface.bulkDelete('Grades', [
				{
					nom: 'Lieutenant',
					short :'ltn',
					grade : 1
				},
				{
					nom: 'Adjudant-chef',
					short :null,
					grade : 2
				},
				 {
				 	nom: 'Adjudant',
					short :null,
					grade : 3
				},
				{
					nom: 'sergent-chef',
					short :'sgt-chef',
					grade : 4
				},
				{
					nom: 'sergent',
					short :'srg',
					grade : 5
				},
				{
					nom: 'caporal-chef',
					short :null,
					grade : 6
				},
				{
					nom: 'caporal',
					short :null,
					grade : 7
				},
				{
					nom: 'première Classe',
					short :null,
					grade : 8
				},
				{
					nom: 'seconde Classe',
					short :null,
					grade : 9
				},
			], {});		
		/*
			Add reverting commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkDelete('People', null, {});
		*/
	}
};
