'use strict';
var bcryptjs = require('bcryptjs');


var saltRounds = 10;


module.exports = {
	
	up: (queryInterface, Sequelize) => {
		var salt = bcryptjs.genSaltSync(saltRounds);
		var password = bcryptjs.hashSync("password", salt);
		   return queryInterface.bulkInsert('Users', [{
				nom: 'Super',
				prenom : 'Admin',
				email : 'admin@mail.com',
				avatar : null,
				mdp : password,
				Profils_id : 1,
				Grades_id : 1,
				CreatedAt: new Date(),
    			UpdatedAt: new Date()
			}], {});
			
		
		/*
			Add altering commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkInsert('People', [{
				name: 'John Doe',
				isBetaMember: false
			}], {});
		*/
	},

	down: (queryInterface, Sequelize) => {

		return queryInterface.bulkDelete('Users', [
			{nom: 'Super', prenom : 'Admin'}
			], {})

		/*
			Add reverting commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkDelete('People', null, {});
		*/
	}
};
