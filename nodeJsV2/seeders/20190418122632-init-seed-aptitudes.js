'use strict';

module.exports = {
	up: (queryInterface, Sequelize) => {

		return queryInterface.bulkInsert('Aptitudes', [
			{
				nom: 'Chef d\'agès tout engin',
				short: null
			},
			{
				nom: 'Conducteur poids lourds',
				short: null
			},
			{
				nom: 'Chef d\'agès une équipe',
				short: null
			},
			{
				nom: 'chef d\'équipe',
				short: null
			},
			{
				nom: 'conducteur vl',
				short: null
			}
			], {});

		/*
			Add altering commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkInsert('People', [{
				name: 'John Doe',
				isBetaMember: false
			}], {});
		*/
	},

	down: (queryInterface, Sequelize) => {

		return queryInterface.bulkDelete('Aptitudes', [
			{
				nom: 'Chef d\'agès tout engin',
				short: null
			},
			{
				nom: 'Conducteur poids lourds',
				short: null
			},
			{
				nom: 'Chef d\'agès une équipe',
				short: null
			},
			{
				nom: 'chef d\'équipe',
				short: null
			},
			{
				nom: 'conducteur vl',
				short: null
			}
			], {});
		/*
			Add reverting commands here.
			Return a promise to correctly handle asynchronicity.

			Example:
			return queryInterface.bulkDelete('People', null, {});
		*/
	}
};
