'use strict';
module.exports = {
		up: (queryInterface, Sequelize) => {
				return queryInterface.createTable('Commentaires', {
						id: {
								allowNull: false,
								autoIncrement: true,
								primaryKey: true,
								type: Sequelize.INTEGER.UNSIGNED
						},
						texte: {
								allowNull: false,
								type: Sequelize.TEXT
						},
						Posts_id: {
								allowNull: false,
								type: Sequelize.INTEGER.UNSIGNED,
								onDelete: 'CASCADE',
								references:{
									model:'Posts',
									key: 'id',
								}
						},
						Users_id: {
								allowNull: false,
								type: Sequelize.INTEGER.UNSIGNED,
								onDelete: 'CASCADE',
								references:{
									model:'Users',
									key: 'id',
								}
						},
						createdAt: {
								allowNull: false,
								type: Sequelize.DATE
						},
						updatedAt: {
								allowNull: false,
								type: Sequelize.DATE
						},
				});
		},
		down: (queryInterface, Sequelize) => {
				return queryInterface.dropTable('Commentaires');
		}
};