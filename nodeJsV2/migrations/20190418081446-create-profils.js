'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Profils', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			nom: {
				allowNull: false,
				type: Sequelize.STRING(20)
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Profils');
	}
};