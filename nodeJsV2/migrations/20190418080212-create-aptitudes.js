'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Aptitudes', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER().UNSIGNED
			},
			nom: {
				allowNull: false,
				type: Sequelize.STRING(100)
			},
			short: {
				type: Sequelize.STRING(10)
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Aptitudes');
	}
};