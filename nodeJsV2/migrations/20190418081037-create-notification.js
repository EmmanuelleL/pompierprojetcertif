'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Notifications', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			Messages_id: {
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'MessagesNotifs',
					key: 'id',
				}
			},
			Posts_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				onDelete: 'CASCADE',
				references:{
					model:'Posts',
					key: 'id',
				}
			},
			Users_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				onDelete: 'CASCADE',
				references:{
					model:'Users',
					key: 'id',
				}
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Notifications');
	}
};