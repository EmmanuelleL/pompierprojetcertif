'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Posts_has_Responses', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			horaire_debut: {
				allowNull: false,
				type: Sequelize.DATE
			},
			horaire_fin: {
				allowNull: false,
				type: Sequelize.DATE
			},
			Users_id: {
				allowNull: false,
				onDelete: 'CASCADE',
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id',
				}
			},
			Posts_id: {
				allowNull: false,
				onDelete: 'CASCADE',
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Posts',
					key: 'id',
				}
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Posts_has_Responses');
	}
};