'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('MessagesNotifs', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			message: {
				allowNull: false,
				type: Sequelize.STRING(100)
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('MessagesNotifs');
	}
};