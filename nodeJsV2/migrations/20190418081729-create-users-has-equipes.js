'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Users_has_Equipes', {
			Equipes_id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Equipes',
					key: 'id',
				}
			},
			Users_id: {
				allowNull: false,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id',
				}
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Users_has_Equipes');
	}
};