'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Equipes', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			numero: {
				 allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED
			},
			Responsables_id: {
				 allowNull: true,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id',
				}
			},
			Casernes_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Casernes',
					key: 'id',
				}
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Equipes');
	}
};