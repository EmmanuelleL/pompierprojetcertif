'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Grades', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			nom: {
				 allowNull: false,
				type: Sequelize.STRING(100)
			},
			short: {
				type: Sequelize.STRING(10)
			},
			grade: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Grades');
	}
};