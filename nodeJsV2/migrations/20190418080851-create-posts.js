'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Posts', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			horaire_debut: {
				allowNull: false,
				type: Sequelize.DATE
			},
			horaire_fin: {
				allowNull: false,
				type: Sequelize.DATE
			},
			Users_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id',
				}
			},
			Sujets_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Sujets',
					key: 'id',
				}
			},
			Casernes_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Casernes',
					key: 'id',
				}
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE
			}
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Posts');
	}
};