'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Casernes', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER.UNSIGNED
			},
			nom: {
				allowNull: false,
				type: Sequelize.STRING(100)
			},
			adresse: {
				allowNull: false,
				type: Sequelize.STRING
			},
			Responsables_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id'
				}
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Casernes');
	}
};