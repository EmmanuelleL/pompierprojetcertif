'use strict';
module.exports = {
	up: (queryInterface, Sequelize) => {
		return queryInterface.createTable('Users', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			nom: {
				allowNull: false,
				type: Sequelize.STRING(100)
			},
			prenom: {
				allowNull: false,
				type: Sequelize.STRING(50)
			},
			email: {
				allowNull: false,
				type: Sequelize.STRING
			},
			avatar: {
				type: Sequelize.STRING
			},
			mdp: {
				allowNull: false,
				type: Sequelize.STRING
			},
			Profils_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Profils',
					key: 'id',
				}
			},
			Grades_id: {
				allowNull: false,
				type: Sequelize.INTEGER.UNSIGNED,
				references:{
					model:'Users',
					key: 'id',
				}
			},
			token: {
				type: Sequelize.STRING
			},
			createdAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updatedAt: {
				allowNull: false,
				type: Sequelize.DATE
			},
		});
	},
	down: (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Users');
	}
};