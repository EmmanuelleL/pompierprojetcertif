var express = require('express');
var userCtrl = require('../controlleur/UserCtrl');
var casernesCtrl= require('../controlleur/CasernesCtrl');
var equipesCtrl= require('../controlleur/EquipesCtrl');
var postsCtrl= require('../controlleur/PostsCtrl');
var notifsCtrl= require('../controlleur/NotifsCtrl');

var commentairesCtrl= require('../controlleur/CommentairesCtrl');
var models = require('../models')

var passport = require('passport');
var BearerStrategy = require('passport-http-bearer');



exports.router = (function () {
	
	var apiRouter = express.Router();
	///////////////
	//auth route
	/////////////////
	apiRouter.route('/user/').post(userCtrl.save);
	apiRouter.route('/login/').post(userCtrl.login);
	apiRouter.post('/logout/', passport.authenticate('bearer', { session: false }),userCtrl.logout);
	
	////////////////
	//Users route
	////////////////

	//recupere un utilisateur avec son id
	apiRouter.get("/user/:id", passport.authenticate('bearer', { session: false }), userCtrl.getbyid)
	//recuppere l'utilisateur connecter
	apiRouter.get("/user", passport.authenticate('bearer', { session: false }), userCtrl.getUser)
	//recupere les equipe de l'utilisateur connecter
	apiRouter.get("/user/:id/equipe", passport.authenticate('bearer', { session: false }), userCtrl.getEquipe)
	//reccupere tous les utilisateur
	apiRouter.get("/users",  passport.authenticate('bearer', { session: false }), userCtrl.getAll)
	//reccupere tous les profils existants
	apiRouter.get("/users/profils",  passport.authenticate('bearer', { session: false }), userCtrl.getAllProfils)
	//reccupere tous les grades existants
	apiRouter.get("/users/grades",  passport.authenticate('bearer', { session: false }), userCtrl.getAllGrades)
	//modifie un utilisateur
	apiRouter.put("/user/:id/update",  passport.authenticate('bearer', { session: false }), userCtrl.update)
	// supprimer un utilisateur
	apiRouter.delete("/user/:id/delete",  passport.authenticate('bearer', { session: false }), userCtrl.delete)
	//gestion des aptitudes
		//reccupere tous les aptitudes existantes
		apiRouter.get("/users/aptitudes",  passport.authenticate('bearer', { session: false }), userCtrl.getAllAptitudes)
		//reccupere les aptitudes d'un utilisateur
		apiRouter.get("/user/:id/aptitudes",  passport.authenticate('bearer', { session: false }), userCtrl.getUserAptitudes)
		//ajoute des aptitude a un utilisateur
		apiRouter.post("/users/aptitudes", passport.authenticate('bearer', { session: false }), userCtrl.addAptitudes)
		//suprime une aptitudes lier a un utilisateur
		apiRouter.delete("/users/aptitudes", passport.authenticate('bearer', { session: false }), userCtrl.deleteAptitudes) 

	//////////////
	//Posts route
	///////////////
	//reccupere les poste d'une caserne
	apiRouter.get("/post", passport.authenticate('bearer', { session: false }), postsCtrl.getAll) 
	//recupere les poste d'un utilsateur
	apiRouter.get("/post/:id", passport.authenticate('bearer', { session: false }), postsCtrl.getUserPosts)
	//recuppere un post avec des information detaillé
	apiRouter.get ("/post/:id/detailPost", passport.authenticate('bearer', { session: false }), postsCtrl.getDetailPosts)
	//ajoute un post
	apiRouter.post("/post/ajouter", passport.authenticate('bearer', { session: false }), postsCtrl.save)
	//modifie un post
	apiRouter.put("/post/:id/update", passport.authenticate('bearer', { session: false }), postsCtrl.update)
	//supprime un post
	apiRouter.delete("/post/:id", passport.authenticate('bearer', { session: false }), postsCtrl.delete)
	//repondre a un post
	apiRouter.post("/post/:id/repondre", passport.authenticate('bearer', { session: false }), postsCtrl.repondre)

	//////////////
	//Notification route
	//////////////
	apiRouter.get("/user/:id/notif", passport.authenticate('bearer', { session: false }), notifsCtrl.getAll)
	apiRouter.post("/post/:id/notif", passport.authenticate('bearer', { session: false }), notifsCtrl.save)
	apiRouter.get ("/post/:id/notif", passport.authenticate('bearer', { session: false }), notifsCtrl.getPostNotifs)
	apiRouter.delete("/post/:id/notif", passport.authenticate('bearer', { session: false }), notifsCtrl.delete)

	/////////////
	//commentaires route
	////////////
	//ajoute un commentaire
	apiRouter.post("/post/:id/commentaire", passport.authenticate('bearer', { session: false }), commentairesCtrl.save)



	//////////////
	//casernes route
	///////////////
	//recupere toutes les casernes
	apiRouter.get("/casernes", passport.authenticate('bearer', { session: false }), casernesCtrl.getAll) 
	//recupere les casernes d'un utilisateur
	apiRouter.get("/casernes/user", passport.authenticate('bearer', { session: false }), casernesCtrl.getCasernesUser)
	//recupere les utilisateur d'une caserne
	apiRouter.get("/users/caserne/:id", passport.authenticate('bearer', { session: false }), casernesCtrl.getUsersCaserne)
	//ajoute une caserne
	apiRouter.post("/casernes", passport.authenticate('bearer', { session: false }), casernesCtrl.save)
	//suppime une caserne 
	apiRouter.delete('/casernes/:id', passport.authenticate('bearer', { session: false }), casernesCtrl.delete)

	//////////////
	//equipes route
	///////////////
	//recupere les equipes d'une caserne
	apiRouter.get("/equipes", passport.authenticate('bearer', { session: false }), equipesCtrl.getAll)
	//recupere une equipe avec son id
	apiRouter.get("/equipes/:id", passport.authenticate('bearer', { session: false }), equipesCtrl.getbyid)
	// recupere les utilisateur d'une equipes
	apiRouter.get("/users/equipe/:id", passport.authenticate('bearer', { session: false }), equipesCtrl.getUsers)
	//ajoute une equipe
	apiRouter.post("/equipes", passport.authenticate('bearer', { session: false }), equipesCtrl.save)
	//modifie une equipe
	apiRouter.put("/equipes/:id/update", passport.authenticate('bearer', { session: false }), equipesCtrl.update)
	//ajoute une equipe à un utilisateur
	apiRouter.post("/users/equipe",passport.authenticate('bearer', { session: false }), equipesCtrl.addUser)
	//supprime une equipe
	apiRouter.delete("/equipes/:id",passport.authenticate('bearer', { session: false }), equipesCtrl.delete)
	return apiRouter;


})();