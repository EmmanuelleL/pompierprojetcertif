'use strict';
module.exports = (sequelize, DataTypes) => {
  const Notifications = sequelize.define('Notifications', {
    Messages_id: DataTypes.INTEGER.UNSIGNED,
    Posts_id: DataTypes.INTEGER.UNSIGNED,
    Users_id: DataTypes.INTEGER.UNSIGNED
  }, { timestamps: false,
  	classMethods : {
	  	associate : function(models) {
		    models.Notification.belongsTo(models.MessageNotifs,{
					foreignKey :{
						allowNull : false,
					}
				})
		    models.Notification.belongsTo(models.Posts,{
					foreignKey :{
						allowNull : false,
					}
				})
		    models.Notification.belongsTo(models.Users,{
					foreignKey :{
						allowNull : false,
					}
				})
			}
		}
	});
  
  return Notifications;
};