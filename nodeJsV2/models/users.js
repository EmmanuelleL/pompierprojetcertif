'use strict';
module.exports = (sequelize, DataTypes) => {
	const Users = sequelize.define('Users', {
		nom: DataTypes.STRING,
		prenom: DataTypes.STRING,
		email: DataTypes.STRING,
		avatar: DataTypes.STRING,
		mdp: DataTypes.STRING,
		Profils_id: DataTypes.INTEGER.UNSIGNED,
		Grades_id: DataTypes.INTEGER.UNSIGNED,
		token: DataTypes.STRING
	}, {});

	
	return Users;
};