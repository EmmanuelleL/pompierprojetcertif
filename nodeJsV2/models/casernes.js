'use strict';
module.exports = (sequelize, DataTypes) => {
  const Casernes = sequelize.define('Casernes', {
    nom: DataTypes.STRING,
    adresse: DataTypes.STRING,
    Responsables_id: DataTypes.INTEGER.UNSIGNED
  }, { timestamps: false });
  
  return Casernes;
};