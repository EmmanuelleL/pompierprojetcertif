'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users_has_Aptitudes = sequelize.define('Users_has_Aptitudes', {
    Aptitudes_id: DataTypes.INTEGER.UNSIGNED,
    Users_id: DataTypes.INTEGER.UNSIGNED
  },  { timestamps: false });
  Users_has_Aptitudes.associate = function(models) {
    // associations can be defined here
  };
  return Users_has_Aptitudes;
};