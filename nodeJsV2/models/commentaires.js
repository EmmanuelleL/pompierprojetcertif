'use strict';
module.exports = (sequelize, DataTypes) => {
	const Commentaires = sequelize.define('Commentaires', {
		texte: DataTypes.TEXT,
		Posts_id: DataTypes.INTEGER.UNSIGNED,
		Users_id: DataTypes.INTEGER.UNSIGNED
	}, {});
	
	return Commentaires;
};