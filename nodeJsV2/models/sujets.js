'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sujets = sequelize.define('Sujets', {
    nom: DataTypes.STRING
  }, { timestamps: false });
  Sujets.associate = function(models) {
    // associations can be defined here
  };
  return Sujets;
};