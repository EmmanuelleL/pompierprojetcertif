require('./users.js')
'use strict';
module.exports = (sequelize, DataTypes) => {
  const Equipes = sequelize.define('Equipes', {
    numero: DataTypes.INTEGER.UNSIGNED,
    Responsables_id: DataTypes.INTEGER.UNSIGNED,
    Casernes_id: DataTypes.INTEGER.UNSIGNED
  },  { timestamps: false });


// sequelize.models.Equipes.belongsTo(sequelize.models.Users,{
// 		as :'Responsables',
// 		foreignKey :'Responsables_id'
// 	})

// sequelize.models.Equipes.belongsTo(sequelize.models.Casernes,{
// 		as :'Caserne',
// 		foreignKey :'Casernes_id'
// 	})

	return Equipes;
};