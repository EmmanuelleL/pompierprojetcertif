'use strict';
module.exports = (sequelize, DataTypes) => {
  const MessagesNotif = sequelize.define('MessagesNotifs', {
    message: DataTypes.STRING
  }, {});
  MessagesNotif.associate = function(models) {
    // associations can be defined here
  };
  return MessagesNotif;
};