'use strict';
module.exports = (sequelize, DataTypes) => {
  const Grades = sequelize.define('Grades', {
    nom: DataTypes.STRING,
    short: DataTypes.STRING,
    grade: DataTypes.INTEGER.UNSIGNED
  }, { timestamps: false });
  Grades.associate = function(models) {
    // associations can be defined here
  };
  return Grades;
};