'use strict';
module.exports = (sequelize, DataTypes) => {
  const Users_has_Equipes = sequelize.define('Users_has_Equipes', {
    Equipes_id: DataTypes.INTEGER.UNSIGNED,
    Users_id: DataTypes.INTEGER.UNSIGNED
  }, { timestamps: false });



sequelize.models.Casernes.belongsTo(sequelize.models.Users, {foreignKey: "Responsables_id"})
sequelize.models.Users.hasMany(sequelize.models.Casernes, {as : 'Responsable_Caserne', foreignKey: "Responsables_id"})

sequelize.models.Casernes.hasMany(sequelize.models.Equipes, {foreignKey : "Casernes_id"})
sequelize.models.Equipes.belongsTo(sequelize.models.Casernes, {foreignKey : "Casernes_id"})

sequelize.models.Equipes.belongsTo(sequelize.models.Users, {as : 'Responsable_Equipe',foreignKey: "Responsables_id"})
sequelize.models.Users.hasMany(sequelize.models.Equipes, {as : 'Responsable_Equipe', foreignKey: "Responsables_id"})

sequelize.models.Posts.hasMany(sequelize.models.Commentaires,{foreignKey :"Posts_id"})
sequelize.models.Commentaires.belongsTo(sequelize.models.Posts,{foreignKey :"Posts_id"})

sequelize.models.Posts.hasMany(sequelize.models.Posts_has_Response,{foreignKey :"Posts_id"})

sequelize.models.Commentaires.belongsTo(sequelize.models.Users, {foreignKey: "Users_id"})

sequelize.models.Users_has_Equipes.belongsTo(sequelize.models.Users,{
		as : "Users",
		foreignKey: 'Users_id',
	})


sequelize.models.Users.belongsTo(sequelize.models.Profils,{	foreignKey: 'Profils_id'})
	sequelize.models.Users.belongsTo(sequelize.models.Grades,{foreignKey: 'Grades_id'})
	sequelize.models.Users.belongsToMany(sequelize.models.Aptitudes, {
					through: 'Users_has_Aptitudes', 
					foreignKey: 'Users_id' 
				})
	sequelize.models.Aptitudes.belongsToMany(sequelize.models.Users, {
					through: 'Users_has_Aptitudes', 
					foreignKey: 'Aptitudes_id' 
				})
	sequelize.models.Users.belongsToMany(sequelize.models.Equipes, {
					as :"Equipe",
					through: 'Users_has_Equipes', 
					foreignKey: 'Users_id' 
				})
	sequelize.models.Equipes.belongsToMany(sequelize.models.Users, {
				as :'Equipe',
				through: 'Users_has_Equipes', 
				foreignKey: 'Equipes_id' 
			})
	
  return Users_has_Equipes;
};