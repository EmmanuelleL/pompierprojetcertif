'use strict';
module.exports = (sequelize, DataTypes) => {
	const Posts_has_Response = sequelize.define('Posts_has_Response', {
		horaire_debut: DataTypes.DATE,
		horaire_fin: DataTypes.DATE,
		Users_id: DataTypes.INTEGER,
		Posts_id: DataTypes.INTEGER
	}, { timestamps: false });
	Posts_has_Response.associate = function(models) {
		models.Posts_has_Response.belongsTo(models.Posts,{
	    	foreignKey :"Posts_id"
	    }) 
	    models.Posts_has_Response.belongsTo(models.Users,{
	    	foreignKey :"Users_id"
	    })  
	};
	return Posts_has_Response;
};