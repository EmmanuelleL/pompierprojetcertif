'use strict';
module.exports = (sequelize, DataTypes) => {
  const Aptitudes = sequelize.define('Aptitudes', {
    nom: DataTypes.STRING,
    short: DataTypes.STRING
  },  { timestamps: false });
  Aptitudes.associate = function(models) {
    // associations can be defined here
  };
  return Aptitudes;
};