'use strict';
module.exports = (sequelize, DataTypes) => {
  	const Posts = sequelize.define('Posts', {
  	  	horaire_debut: DataTypes.DATE,
  	  	horaire_fin: DataTypes.DATE,
  	  	Users_id: DataTypes.INTEGER.UNSIGNED,
  	  	Sujets_id: DataTypes.INTEGER.UNSIGNED,
  	  	Casernes_id: DataTypes.INTEGER.UNSIGNED
  	}, {});
  	Posts.associate = function(models) {
  	  	models.Posts.belongsTo(models.Users,{
  	  	  	foreignKey :"Users_id"
  	  	})
  	    models.Posts.belongsTo(models.Sujets,{
          as:'Sujets',
			foreignKey :"Sujets_id"
		})
	    models.Posts.belongsTo(models.Casernes,{
			foreignKey :"Casernes_id"
		})  

  	};
  	return Posts;
};