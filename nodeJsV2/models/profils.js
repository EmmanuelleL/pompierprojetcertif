'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profils = sequelize.define('Profils', {
    nom: DataTypes.STRING
  }, { timestamps: false });
  Profils.associate = function(models) {
    // associations can be defined here
  };
  return Profils;
};