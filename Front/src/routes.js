export default {
  '\/$': 'Accueil',
  '\/?NouveauPost\/?$': 'NouveauPost',
  '\/?MesPosts\/?$': 'MesPosts',
  '\/?MonCompte\/?$': 'MonCompte',
  '\/?LesRemplacements\/?$': 'LesRemplacements',
  '\/?MesRemplacements\/?$': 'MesRemplacements',
  '\/?MesPosts|LesRemplacements/[0-9]+/DetailPost\/?$': 'DetailPost',
  '\/?GestionCaserne\/?$': 'GestionCaserne',
  '\/?GestionEquipe\/?$': 'GestionEquipe',
  '\/?GestionMembre\/?$': 'GestionMembre',
  '\/?GestionUtilisateur\/?$' : 'GestionUtilisateur',
  '\/?Utilisateur/[0-9]+\/?$' : 'Utilisateur',

}
