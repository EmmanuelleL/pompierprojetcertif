import Vue from 'vue'
import routes from './routes'
import MesPosts from './pages/MesPosts'
import NouveauPost from './pages/NouveauPost'
import MonCompte from './pages/MonCompte'
import LesRemplacements from './pages/LesRemplacements'
import DetailPost from './pages/DetailPost'
import Accueil from './pages/Accueil'
import GestionCaserne from './pages/GestionCaserne'
import GestionEquipe from './pages/GestionEquipe'
import GestionMembre from './pages/GestionMembre'
import GestionUtilisateur from './pages/GestionUtilisateur'
import Utilisateur from './pages/Utilisateur'

import NoFound from './pages/404'

Vue.config.productionTip = false

const app = new Vue({
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {


      var i = 0;
      var currentR ="";
      for (var route in routes) {
        currentR =new RegExp (Object.keys(routes)[i]);
        if (currentR.exec(this.currentRoute) != null){
          this.currentRoute = Object.keys(routes)[i];
          break;
        }
        i++
      }

      const matchingView = routes[this.currentRoute]
      return matchingView
    }
  },
  beforeDestroy : function(){
    if(!sessionStorage.token  && localStorage.token){
      localStorage.clear();
    }
  },
  render (h) {
    if (this.ViewComponent == "MesPosts"){
      return h(MesPosts)
     }else if (this.ViewComponent == "NouveauPost"){
      return h(NouveauPost)
     }else if (this.ViewComponent == "DetailPost"){
      return h(DetailPost)
    }else if (this.ViewComponent == "MonCompte"){
      return h(MonCompte)
    }else if (this.ViewComponent == "LesRemplacements"){
      return h(LesRemplacements)
     }else if (this.ViewComponent == "Accueil"){
      return h(Accueil)
     }else if (this.ViewComponent == "GestionCaserne"){
      return h(GestionCaserne)
     }else if (this.ViewComponent == "GestionEquipe"){
      return h(GestionEquipe)
     }else if (this.ViewComponent == "GestionMembre"){
      return h(GestionMembre)
     }else if (this.ViewComponent == "GestionUtilisateur"){
      return h(GestionUtilisateur)
     }else if (this.ViewComponent == "Utilisateur"){
      return h(Utilisateur)
     }else{
      return h(NoFound)
     }
     
  }
}).$mount('#app');

window.addEventListener('popstate', () => {
  app.currentRoute = window.location.pathname
})

$('.navbar-toggler').click(function(){
  $('.content').toggleClass('margin-left');
});

$('.commantaire p').click(function(){
  $(this).toggleClass('text-truncate');
});


  var el = document.querySelector("#app header");
var hight = el.offsetHeight;
   $(window).scroll(function () {//Au scroll dans la fenetre on déclenche la fonction
      if ($(this).scrollTop() > hight) { //si on a défilé de plus de 160px du haut vers le bas
        $('nav').addClass("fixed"); //on ajoute la classe "fixe" au header
        $('.content').addClass("pt-scroll");
        $('header').hide();
      } else {
        $('nav').removeClass("fixed");//sinon on retire la classe "fixe" (c'est pour laisser le header à son endroit de départ lors de la remontée
        $('.content').removeClass("pt-scroll");
        $('header').show();
      }
   });

   
