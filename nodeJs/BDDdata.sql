INSERT INTO `Grades` ( `nom`, `short`) VALUES ( 'Lieutenant', 'LTN'), ( 'Sergent', 'SGT'), ( '1ere classe', 'SPV');
INSERT INTO `Aptitudes` ( `nom`, `short`) VALUES ( 'Conducteur poids lourds', 'PL'), ( 'Chef d\'intervention', 'CI');
INSERT INTO `Sujet` ( `nom`) VALUES ( 'Demande de remplacement'), ( 'Proposition de remplacement');
INSERT INTO `Profils` ( `nom`) VALUES ( 'Super admin'), ( 'Responsable caserne'), ( 'Responsable équipe');
INSERT INTO `users` (`nom`, `prenom`, `email`, `password`, `Profils_id`, `Grades_id`) VALUES ( 'nom', 'prenom', 'mail@mail.com', 'password',  '1', '1');
INSERT INTO `casernes` (`nom`, `adresse`, `Responsable_id`) VALUES ('Bruz', '123 rue somewhere 35000 BRUZ', '1');
INSERT INTO `equipes` (`id`, `numero`, `Responsable_id`, `Casernes_id`) VALUES (NULL, '4', '1', '1');
INSERT INTO `equipes_has_users`(`Equipes_id`, `Users_id`) VALUES (1,1);
INSERT INTO `users_has_aptitudes` (`Users_id`, `Aptitudes_id`) VALUES ('1', '1');