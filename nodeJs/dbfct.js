
var mysql = require('mysql');
var config = require('./config')
var db = {
	 connect : function(){
		var connection = mysql.createConnection({
			host     : config.host,
			user     : config.user,
			password : config.password,
			database : config.database
		});

		connection.connect();
			connection.on('error', function(err) {
			console.log("[mysql error]",err);
		});

		return connection;
	},

	clearValue(params){
		for (var key in params) {
   if (params.hasOwnProperty(key)) {
      params[key] = params[key].replace(/\s/g,"");
   }
}
		
			return params
	}
}

module.exports = db;