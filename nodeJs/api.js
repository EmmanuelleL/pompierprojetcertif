var http = require('http');
var url = require('url');
var express = require('express');
var bodyParser = require('body-parser'); 
var session = require('cookie-session');
var urlencodedParser = bodyParser.urlencoded({ extended: false });

var app = express();

var mysql = require('mysql');

var cors = require('cors');

var passport = require('passport');

var BearerStrategy = require('passport-http-bearer');

var config = require('./config')

var utilisateur = require('./models/Users')
var Post = require('./models/Posts')
var Caserne = require('./models/Casernes')
var Equipe = require('./models/Equipes')

function findUser(object, next){
		var sql = "SELECT Users.*, Grades.nom as gradeNom,Grades.grade as grade, Profils.nom as profil  FROM `Users`, Grades, Profils WHERE Users.Grades_id = Grades.id AND Users.Profils_id = Profils.id AND token = '"+object.token+"';";

	var connection =connect()
	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			next("erreur");
		}

		data = [];
		for(row in rows){

			// var utilisateur = {};
			// utilisateur.nom = rows[row].nom;
			// utilisateur.prenom = rows[row].prenom;
			// utilisateur.email = rows[row]["e-mail"];
			// utilisateur.ProfilID = rows[row].Profils_id;
			// utilisateur.profil = rows[row].profil;
			// utilisateur.GradeID = rows[row].Grades_id;
			// utilisateur.grade = rows[row].grade;
			// data.push(utilisateur);
			rows[row].mdp =null;

		};
		if(rows.length === 0){
			err= "Unautorize";
			//err.message = "fail"
		}
		next(err,rows)
	});
};

passport.use(new BearerStrategy(
  function(token, done) {
  
	findUser({ token: token }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });

    // User.findOne({ token: token }, function (err, user) {
    //   if (err) { return done(err); }
    //   if (!user) { return done(null, false); }
    //   return done(null, user, { scope: 'all' });
    // });
  }
));

app.use(cors({origin: '*'}))

/////////////
//ajouter a nodeJs v2
//////////////
// recupère tous les post

.get("/post",  function(req, res) {

	Post.getPosts(req, function(posts){
		res.send(posts);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//reccupere les post de l'utilisateur

.get("/post/:id",  function(req, res) {
	var connection = connect()

	var data = [];

	var sql = "SELECT Posts.*, Users.nom, Users.prenom, Posts.Sujet_id , Sujet.nom as sujet FROM Posts, Users, Sujet WHERE Posts.Users_id = Users.id AND  Posts.Sujet_id = Sujet.id AND Users.id = "+ req.params.id +" ORDER BY Posts.id";

	connection.query(sql, function(err, rows, fields) {
			if (err){
				console.log(err)
			}
		  
			for(row in rows){

				var post = {};
				post.id = rows[row].id;
				post.horaire_debut = rows[row].horaire_debut;;
				post.horaire_fin = rows[row].horaire_fin;
				post.Sujet_id = rows[row].Sujet_id;
				post.Sujet = rows[row].sujet;
				post.User = {};
				post.User.nom = rows[row].nom;
				post.User.prenom = rows[row].prenom;
				data.push(post);
				

			};
			res.send(JSON.stringify(data))
			
		});	  	

	
})
/////////////
//ajouter a nodeJs v2
//////////////
//recupere les information détailler de 1 post

.get ("/post/:id/detailPost", function(req, res){
	var connection = connect();
	var data = [];

	var sql = "SELECT Posts.*, Users.nom, Users.prenom, Posts.Sujet_id , Sujet.nom as sujet, Grades.nom as gradeName,Grades.grade as grade, Grades.short as grd, commentaire.id as commentaireID , commentaire.Users_id as commentaireUser,commentaire.texte, commentaire.created_at as commentaireTime , Posts_has_Response.horaire_debut as horaireDebutReponse, Posts_has_Response.horaire_fin as horaireFinReponse, Posts_has_Response.Users_id as userReponse, Posts_has_Response.id as reponseID FROM Users, Sujet, Grades , Posts LEFT JOIN commentaire ON Posts.id = commentaire.Posts_id LEFT JOIN Posts_has_Response ON Posts.id = Posts_has_Response.Posts_id WHERE Posts.Users_id = Users.id AND Posts.Sujet_id = Sujet.id AND Grades.id = Users.Grades_id AND Posts.id =" +req.params.id +" ORDER BY `commentaireID` ASC;";

	connection.query(sql, function(err, rows, fields) {
			if (err){
				console.log(err)
			}
		  	
		  	var postCreate =false;
		  	 var commentaire ={};
		  	 commentaire.id =0;


			for(row in rows){

				if( postCreate === false){
				var post = {};
				postCreate = true;
				post.id = rows[row].id;
				post.horaire_debut = rows[row].horaire_debut;;
				post.horaire_fin = rows[row].horaire_fin;
				post.Sujet_id = rows[row].Sujet_id;
				post.Sujet = rows[row].sujet;
				post.created_at = rows[row].created_at;
				post.User = {};
				post.User.nom = rows[row].nom;
				post.User.prenom = rows[row].prenom;
				post.User.gradeName = rows[row].gradeName;
				post.User.grade = rows[row].grade
				post.User.grd = rows[row].grd;
				post.commentaires = [];
				post.reponses = [];
				data.push(post);
			}
				if (commentaire.id !== rows[row].commentaireID){
					commentaire = {};
					commentaire.texte = rows[row].texte;
					commentaire.id = rows[row].commentaireID;
					commentaire.created_at = rows[row].commentaireTime;
					commentaire.UserID = rows[row].commentaireUser;
					post.commentaires.push(commentaire);
				}	

				var addReponse= true;
				for (var i = 0; i < post.reponses.length; i++) {
					if (post.reponses[i].id == rows[row].reponseID){
						addReponse= false;
					}
				}				
				if(addReponse== true){
					reponse = {};
					reponse.id = rows[row].reponseID;
					reponse.horaire_debut = rows[row].horaireDebutReponse;
					reponse.horaire_fin = rows[row].horaireFinReponse;
					reponse.Users_id = rows[row].userReponse
					post.reponses.push(reponse);
				}
			};
			res.send(data);
			
		});	  



})

/////////////
//ajouter a nodeJs v2
//////////////
//ajoute un posts

.post("/post/ajouter",urlencodedParser, cors(),function(req, res) {

	var connection = connect();

	var sql = "INSERT INTO Posts ( `horaire_debut`, `horaire_fin`, `created_at`, `Users_id`, `Sujet_id`, `Casernes_id`) VALUES ('"+ req.query.horaire_debut+"','"+ req.query.horaire_fin+"', '"+ req.query.time+"','"+ req.query.Users_id+"', '"+ req.query.sujet_id+"', '"+ req.query.Casernes_id+"')"

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.setHeader('Content-Type', 'application/json');
			 res.send(JSON.stringify(rows));
		});


})

/////////////
//ajouter a nodeJs v2
//////////////
//modifier un post avec son id 

.put("/post/:id/update", cors(),function(req, res){
	var connection = connect()

	var sql = "UPDATE `Posts` SET `horaire_debut`='"+req.query.horaire_debut+"',`horaire_fin`='"+req.query.horaire_fin+"',`created_at`='"+req.query.date+"' WHERE id =" +req.params.id;
	

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}

		
			 res.send("succes");
		});

})

/////////////
//ajouter a nodeJs v2
//////////////
//supprime un posts

.delete("/post/:id",function(req, res) {

	var connection =connect();

	var sql = "DELETE FROM Posts WHERE id =" +req.params.id;

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.setHeader('Content-Type', 'application/json');
			 res.send("supprimer");
		});


})

/////////////
//ajouter a nodeJs v2
//////////////
//ajouter un utilisateur

.post("/user",urlencodedParser, cors(), function(req, res){
	utilisateur.save(req, function(row){
		res.send(row);
	})

} )

/////////////
//ajouter a nodeJs v2
//////////////
.post("/login",urlencodedParser, cors(), function(req, res){

	utilisateur.login(req, function(row){
		res.send(row);
	})

} )

/////////////
//ajouter a nodeJs v2
//////////////
.post("/logout", passport.authenticate('bearer', { session: false }), function(req, res){

	utilisateur.logout(req.user, function(row){
		res.send(row);
	})

} )

/////////////
//ajouter a nodeJs v2
//////////////
//reccuperer un utilisateur avec son id 

.get("/user/:id", cors(), passport.authenticate('bearer', { session: false }), function(req, res){
	var connection = connect();

	var sql = "SELECT Users.*, Grades.nom as grade, Profils.nom as profil  FROM `Users`, Grades, Profils WHERE Users.Grades_id = Grades.id AND Users.Profils_id = Profils.id AND Users.id = "+req.params.id;

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}

		data = [];
		for(row in rows){

			rows[row].mdp =null;

		}

		res.setHeader('Content-Type', 'application/json');
			 res.send(JSON.stringify(rows));
		});

})

/////////////
//ajouter a nodeJs v2
//////////////
// reccupere l'utilisateur connecté

.get("/user", cors(), passport.authenticate('bearer', { session: false }), function(req, res){
	res.send(JSON.stringify(req.user));
})

/////////////
//ajouter a nodeJs v2
//////////////

//reccupere l'equipe de l'utilisateur (en fonction de la caserne courrante)

.get("/user/:id/equipe",urlencodedParser, cors(), function(req, res){

	utilisateur.getEquipe(req, function(row){
		res.send(row);
	})

})

/////////////
//ajouter a nodeJs v2
//////////////

//reccupere tous les utilisateurs

.get("/users",urlencodedParser, cors(), function(req, res){

	utilisateur.getUsers(function(row){
		res.send(row);
	})

} )

/////////////
//ajouter a nodeJs v2
//////////////
.get("/users/profils",  function(req, res) {

	utilisateur.getProfils(function(profils){
		res.send(profils);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
.get("/users/grades",  function(req, res) {

	utilisateur.getGrades(function(grades){
		res.send(grades);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
.get("/users/aptitudes",  function(req, res) {

	utilisateur.getAptitudes(function(aptitudes){
		res.send(aptitudes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//reccupere les aptitude d'un utilisateur

.get("/user/:id/aptitudes",function(req, res) {

	utilisateur.getUserAptitudes(req.params.id, function(aptitudes){
		res.send(aptitudes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//ajoute des aptitude a un utilisateur

.post("/users/aptitudes",  function(req, res) {

	utilisateur.addAptitude(req,function(aptitudes){
		res.send(aptitudes);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//supprime des aptitude 

.delete("/user/:id/aptitudes",  function(req, res) {

	utilisateur.deleteAptitude(req,function(aptitudes){
		res.send(aptitudes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//modifier un utilisateur avec son id 

.put("/user/:id/update", cors(),function(req, res){
	var connection = connect();

	if(typeof(req.query.email) == 'undefined'|| req.query.email=== "undefined"){
		email = false;
	}else{
		email = true;
	}
	if(typeof(req.query.avatar) == 'undefined'|| req.query.avatar=== "undefined"){
		avatar = false;
	}else{
		avatar = true;
	}
	if(typeof(req.query.mdp) == 'undefined'|| req.query.mdp=== "undefined"){
		mdp = false;
	}else{
		mdp = true;
	}
	if(typeof(req.query.Profils_id) == 'undefined'|| req.query.Profils_id=== "undefined"){
		Profils_id = false;
	}else{
		Profils_id = true;
	}
	if(typeof(req.query.Grades_id) == 'undefined'|| req.query.Grades_id=== "undefined"){
		Grades_id = false;
	}else{
		Grades_id = true;
	}

	var sql = "UPDATE `Users` SET ";
	email?sql+="`e-mail`='"+req.query.email+"',": "";
	avatar?sql+="`avatar`='"+req.query.avatar+"',": "";
	mdp?sql+="`mdp`='"+req.query.mdp+"',": "";
	Profils_id?sql+="`Profils_id`='"+req.query.Profils_id+"',": "";
	Grades_id?sql+="`Grades_id`='"+req.query.Grades_id+"'": "";
	if(sql[(sql.length -1)]==","){
		sql = sql.slice(0, sql.length -1);
	}
	sql+=" WHERE Users.id = "+req.params.id;

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}

		
			 res.send("succes");
		});

})


//ajout commentaire

.post("/post/:id/commentaire",urlencodedParser, cors(),function(req, res) {

	var connection = connect();

	var sql = "INSERT INTO `commentaire`( `texte`, `created_at`, `Posts_id`, `Users_id`) VALUES ('"+req.query.commentaire+"','"+req.query.created_at+"','"+req.params.id+"','"+req.query.UserId+"')"

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.setHeader('Content-Type', 'application/json');
			 res.send(JSON.stringify(rows));
		});


})

/////////////
//ajouter a nodeJs v2
//////////////
//repondre a un post

.post("/post/:id/repondre",urlencodedParser, cors(),function(req, res) {

	var connection = connect();

	var sql = "INSERT INTO `Posts_has_Response`(`Posts_id`, `Users_id`, `horaire_debut`, `horaire_fin`) VALUES ('"+req.params.id+"','"+req.query.UserId+"','"+req.query.horaire_debut+"','"+req.query.horaire_fin+"')"

	connection.query(sql, function(err, rows, fields) {
		
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.setHeader('Content-Type', 'application/json');
			 res.send(JSON.stringify(rows));
		});


})


//reccuperer toutes les notification d'un user

.get("/user/:id/notif", cors(), function(req, res) {
	function getPostsID(next){
		var connection = connect();
		var sql = "SELECT Posts.id FROM Posts, Users WHERE Posts.Users_id=" +req.params.id
		connection.query(sql, function(err, rows, fields) {
			if (err){
				console.log(err);
				res.send("erreur");
			}else{
				var postsId = [];
				for(row in rows){
					if(typeof(rows[row].id) != "undefined"){
						postsId.push(rows[row].id);
					}
				};
				next(postsId)
			}
		});
	}


	function getPostNotif(postsId, i, next){
		
		var notification = [];
		var connection = connect();
		var sql2= "SELECT `Notification`.id FROM `Notification`  WHERE `Notification`.Posts_id =" + postsId[i];
		connection.query(sql2, function(err, rows, fields) {
			if (err){
			console.log(err);
			res.send("erreur");
			return "erreur";
			}else{
				for(row in rows){
					if(typeof(rows[row].id) != "undefined"){
						notification.push(rows[row].id);
					}
				}
					next(notification);
			}
		});
		
	};
		
			
	
	function getNotifs(next){
		getPostsID( async function(postsId){
		var notif = [];
		for (var i = 0; i < postsId.length; i++) { 
	
			getPostNotif(postsId,i, function(notification){
					var notifications = notification;
					if(typeof(notifications)!= "undefined"){
						notif.push(notifications)
						next(notif);

					}
					if(notif.length === postsId.length){
						next("fin");
					}
				
			})
		}
	});
	}	

	var data = [];
	getNotifs(function(notif){
		
		if(notif == "fin"){
			var result = [];
			for (var i = 0; i < data.length; i++) {
				for (var j = 0; j < data[i].length; j++) {
					if(data[i][j]>=0){
						result.push(data[i][j]);
					}
				}
			}
			res.send(result)
		}else{
			data =notif;
		}	
	})
		
		
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//recuppere les notification d'un post

.get ("/post/:id/notif", function(req, res) {


	var connection = connect();

	var sql = "SELECT `Notification`.id, `MessagesNotif`.message FROM `Notification`, `MessagesNotif` WHERE `MessagesNotif`.id = `Notification`.Messages_id AND `Notification`.Posts_id =" + req.params.id;

	connection.query(sql, function(err, rows, fields) {
		if (err){
			console.log(err);
			res.send("erreur");
		}

		var data = [];

			for(row in rows){

			if (typeof(rows[row].id) != 'undefined'){
				var notification = {};
				notification.id = rows[row].id;
				notification.message = rows[row].message;
				data.push(notification);
			}
		}

		res.send(data);

	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////

//nouvelle notification

.post("/post/:id/notif", function(req, res){
	var connection = connect();
	if(req.query.type == "commentaire"){
		message_id = 1
	}

	if(req.query.type == "reponse"){
		message_id = 2
	}

	var sql = "INSERT INTO `Notification`(`Messages_id`, `Posts_id`) VALUES ("+message_id+","+req.params.id+") ; ";
	connection.query(sql, function(err, rows, fields) {
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.send("ok");
	});
	
})

/////////////
//ajouter a nodeJs v2
//////////////

//suppression de notification 

.delete("/post/:id/notif", function(req, res){
	var connection = connect();
	var sql = "DELETE FROM `Notification` WHERE `Posts_id`=" +req.params.id;
	connection.query(sql, function(err, rows, fields) {
		if (err){
			console.log(err);
			res.send("erreur");
		}
		res.send("ok");
	});
	
})

/////////////
//ajouter a nodeJs v2
//////////////
// recupere toutes les casernes

.get("/casernes",  function(req, res) {

	Caserne.getCasernes(function(casernes){
		res.send(casernes);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
// recupere toutes les casernes

.get("/casernes/responsable",  function(req, res) {

	Caserne.getCasernesResponsable(req.query.id, function(casernes){
		res.send(casernes);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
// recupere toutes les casernes

.get("/casernes/responsableequipe", cors(),  function(req, res) {
	Caserne.getCasernesResponsableRquipe(req.query.id,function(casernes){
		res.send(casernes);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//recupere les caserne de l'utilisateur

.get("/casernes/user", passport.authenticate('bearer', { session: false }), function(req, res) {

	console.log(req.user[0].id)
	utilisateur.getCasernes(req.user[0].id, function(rows){
		console.log(rows)
		res.send(rows);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//reccupere les user de la caserne 

.get("/users/caserne/:id",  function(req, res) {

	Caserne.getUsersCaserne(req.params.id,function(Users){
		res.send(Users);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
// ajoute une caserne

.post("/casernes",  function(req, res) {

	Caserne.save(req, function(casernes){
		res.send(casernes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//reccupere les equipe d'une caserne
.get("/equipes",  function(req, res) {

	Equipe.getEquipes(req,function(equipes){
		res.send(equipes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//reccupere les equipe dont l'utilisateur est responsable
.get("/equipesResponsable",passport.authenticate('bearer', { session: false }),  function(req, res) {

	Equipe.getEquipesResponsable(req.user.id,function(equipes){
		res.send(equipes);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//reccupere une equipe avec son id
.get("/equipes/:id",  function(req, res) {

	Equipe.getEquipeById(req.params.id,function(equipe){
		res.send(equipe);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//recupere les membres d'une equipe

.get("/users/equipe/:id",  function(req, res) {

	Equipe.getUsers(req.params.id,function(equipe){
		res.send(equipe);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//attribut une equipe à un utilisateur

.post("/users/equipe",  function(req, res) {

	Equipe.addUser(req,function(row){
		res.send(row);
	})
	
})

/////////////
//ajouter a nodeJs v2
//////////////
//ajoute une equipe 

.post("/equipes",  function(req, res) {

	Equipe.save(req,function(equipes){
		console.log(equipes)
		res.send(equipes);
	})
	
})
/////////////
//ajouter a nodeJs v2
//////////////
//modifie une equipe 

.put("/equipes/:id/update",  function(req, res) {

	Equipe.update(req,function(equipes){
		console.log(equipes)
		res.send(equipes);
	})
	
})

app.listen(config.port,  ()=>{
	console.log("server started at port "+ config.port);
	});

function connect(){
	var connection = mysql.createConnection({
		host     : config.host,
		user     : config.user,
		password : config.password,
		database : config.database
	});

	connection.connect();
		connection.on('error', function(err) {
		console.log("[mysql error]",err);
	});

	return connection;
}