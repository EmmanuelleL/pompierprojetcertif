var db = require('../dbfct')
var bcryptjs = require('bcryptjs');
var saltRounds = 10;




var Utilisateur = {
	nom  : "",
	prenom  : "",
	email  : "",
	avatar : "",
	ProfilID : 0,
	GradeID  : 0,
	token : "",

	hashMpd : async function(mdp, next){
		bcrypt.hash(mdp, saltRounds, function(err, hash) {
			console.log(hash)
			next(hash);
		});
	},

	generateToken : function(id, next){
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < 60; i++){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}

		var connection = db.connect();
		connection.query("UPDATE `Users` SET `token` = '"+text+"' WHERE `Users`.`id` = "+id+";", function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			

			next(text);
		});

	},

	//hydrate()

	getUsers : function(next){
		var connection = db.connect()
		connection.query("SELECT * FROM Users",function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	},

	//getUserById(id)

	//updateUser(id)

	//deleteUser(id)

	//findUserBy(objet //{{ token: token }}) pour remplacer celle dans apis.js
	getAptitudes : function(next){
		var connection = db.connect()
		connection.query("SELECT * FROM Aptitudes",function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			next(rows);
		});
	},

	getGrades : function(next){
		var connection = db.connect()
		connection.query("SELECT * FROM Grades",function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	},

	getProfils : function(next){
		var connection = db.connect()
		connection.query("SELECT * FROM Profils",function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	},

	// getPost

	// getNotif

	getUserAptitudes : function(id, next){
		var connection = db.connect()
		connection.query("SELECT Aptitudes.* FROM Aptitudes ,`Users_has_Aptitudes`, Users WHERE Users.id = Users_has_Aptitudes.Users_id AND Aptitudes.id = Users_has_Aptitudes.Aptitudes_id AND Users.id = "+id,function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			next(rows);
		});
	},

	getEquipe : function(req, next){
		var connection = db.connect()
		connection.query("SELECT Equipes.id FROM `Equipes`, Users_has_Equipes, Users WHERE Users_has_Equipes.Users_id = Users.id AND Users_has_Equipes.Equipes_id = Equipes.id AND Users.id ="+req.params.id+" AND Equipes.Casernes_id = " + req.query.Casernes_id,function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			next(rows);
		});
	},

	save : async function (req, next) {
		req.query =  db.clearValue(req.query);
		this.hashMpd(req.query.mdp, function(mdp){
			var connection = db.connect();
		var sql = "INSERT INTO `Users`( `nom`, `prenom`, `e-mail`, `avatar`, `mdp`, `Profils_id`, `Grades_id`) VALUES (?,?,?,?,?,?,?)"
		connection.query(sql, [req.query.nom, req.query.prenom, req.query.email, req.query.avatar,mdp, req.query.Profils_id, req.query.Grades_id], function(err, rows, fields) {
				if (err){
					console.log(err);
					next("erreur");
				}

				
				next(rows);
			})
		});
	},
	
	login : function(req, next){

			var connection = db.connect();
			var sql = "SELECT id, mdp FROM `Users` WHERE `e-mail` = '" +req.query.email +"';"
			var classe= this;
			connection.query(sql, function(err, rows, fields) {
				if (err){
					console.log(err);
					next("erreur");
				}
				var hash = rows[0].mdp
				var id = rows[0].id
				bcrypt.compare(req.query.mdp, hash, function(err, res) {
				    if(res == true){
				    	classe.generateToken(id, function(token){

				   		 
							next(token)
						})
				    }else{
				    	console.log("wrong login")

				    	
				    	next("erreur")
				    }
				});
		});
	},

	logout : function (User, next){
		var connection = db.connect();
		connection.query("UPDATE `Users` SET `token` = NULL WHERE `Users`.`id` = "+User[0].id+";", function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next("decconecté");
		});
	},

	getCasernes : function(Users_id, next){
		var connection = db.connect();
		var sql = "SELECT Casernes.nom, Casernes.id, Casernes.adresse FROM `Equipes`, Casernes, Users, Users_has_Equipes WHERE Users.id = Users_has_Equipes.Users_id AND Equipes.id = Users_has_Equipes.Equipes_id AND Equipes.Casernes_id = Casernes.id AND Users.id = "+Users_id;
		connection.query(sql, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	},
	addAptitude : function(req, next){
		var connection = db.connect();
		connection.query("INSERT INTO `Users_has_Aptitudes`(`Users_id`, `Aptitudes_id`) VALUES (?,?)",[req.query.Users_id,req.query.Aptitudes_id], function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	},

	deleteAptitude : function(req, next){
		var connection = db.connect();
		connection.query("DELETE FROM `Users_has_Aptitudes` WHERE Users_id="+req.params.id+" AND Aptitudes_id ="+req.query.Aptitudes_id, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}

			
			next(rows);
		});
	}
};



module.exports = Utilisateur;

