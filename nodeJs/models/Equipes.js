var db = require('../dbfct')

var Equipe = {
	id : 0,
	numero : 0,
	responsable_id : 0,
	Casernes_id : 0,

	getUsers: function(id, next){
		var connection = db.connect();
		connection.query("SELECT Users.* FROM `Users_has_Equipes`, Users, Equipes WHERE Users_has_Equipes.Users_id = Users.id AND Users_has_Equipes.Equipes_id = Equipes.id AND Equipes.id ="+id, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	addUser : function(req, next){
		var connection = db.connect();
		connection.query("INSERT INTO `Users_has_Equipes`(`Users_id`, `Equipes_id`) VALUES (?,?)",[req.query.Users_id,req.query.Equipes_id], function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	getEquipeById : function(id, next){
		var connection = db.connect();
		connection.query("SELECT * FROM `Equipes` WHERE id="+id, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	getEquipes : function (req,next) {
		var connection = db.connect();
		connection.query("SELECT * FROM `Equipes` WHERE Casernes_id="+req.query.Casernes_id, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	getEquipesResponsable : function(id, next){
		var connection = db.connect();
		connection.query("SELECT * FROM `Equipes` WHERE `Responsable_id`="+id, function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	save : function(req, next){
		var connection = db.connect();
		connection.query("INSERT INTO `Equipes`( `numero`, `Responsable_id`, `Casernes_id`) VALUES ("+req.query.numero+","+req.query.responsable+", "+req.query.Casernes_id+")",function(err, rows, fields) {
			if (err){
					console.log(err);
					next("erreur");
				}
			
			connection.end();
			next(rows);
			
		});
	},

	update : function(req, next){
		var connection = db.connect();
		connection.query("UPDATE `Equipes` SET `numero`=?,`Responsable_id`=?,`Casernes_id`=? WHERE id="+req.params.id, [req.query.numero,req.query.responsable,req.query.Casernes_id],function(err, rows, fields){
			if (err){
					console.log(err);
					next("erreur");
				}
			next(rows);
		});
	},
}

module.exports = Equipe;