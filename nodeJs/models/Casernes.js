var db = require('../dbfct')

var Caserne = {
	id : 0,
	nom : "",
	adresse : "",

	getCasernes : function (next) {
		var connection = db.connect();
		connection.query("SELECT * FROM `Casernes` ", function(err, rows, fields) {
			if (err){
					console.log(err);
					connection.end();
					next("erreur");
				}
			connection.end();
			next(rows);
		});
	},

	getCasernesResponsable : function(id, next){
		var connection = db.connect();
		connection.query("SELECT * FROM `Casernes` WHERE `Responsable_id`= "+ id, function(err, rows, fields) {
			if (err){
					console.log(err);
					connection.end();
					next("erreur");
				}
			connection.end();
			next(rows);
		});
	},

	getCasernesResponsableRquipe: function(id, next){
		var connection = db.connect();
		connection.query("SELECT * FROM `Casernes`, Equipes WHERE Equipes.Casernes_id = Casernes.id AND Equipes.Responsable_id ="+ id, function(err, rows, fields) {
			if (err){
					console.log(err);
					connection.end();
					next("erreur");
				}
			connection.end();
			next(rows);
		});
	},

	getUsersCaserne : function(id, next){
		var connection = db.connect();
		var sql = "SELECT Users.* FROM `Equipes`, Casernes, Users, Users_has_Equipes WHERE Users.id = Users_has_Equipes.Users_id AND Equipes.id = Users_has_Equipes.Equipes_id AND Equipes.Casernes_id = Casernes.id AND Casernes.id = " + id
		connection.query(sql, function(err, rows, fields){
			if (err){
					console.log(err);
					connection.end();
					next("erreur");
				}
			connection.end();
			next(rows);
		})
	},

	save : function(req, next){
		var connection = db.connect();
		connection.query("INSERT INTO `Casernes`( `nom`, `adresse`, `Responsable_id`) VALUES (?,?,?)", [req.query.nom,req.query.adresse,req.query.responsable],function(err, rows, fields) {
			if (err){
					console.log(err);
					connection.end();
					next("erreur");
				}
			connection.end();
			next(rows);
		});
	},

	
}

module.exports = Caserne;