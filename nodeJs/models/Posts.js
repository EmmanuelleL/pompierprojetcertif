var db = require('../dbfct')

var Post = {

	id : 0,
	horaire_debut : "",
	horaire_fin : "",
	Sujet_id : 0,
	sujet : "",
	Users_id : 0,
	User : {
		nom : "",
		prenom : "",
		Grades_id : 0,
		grade : "",
		grd : "",
	},

	getPosts : function(req, next){
		var connection = db.connect()

		var data = [];

		var sql = "SELECT Posts.*, Users.nom, Users.prenom, Grades.nom as gradeNom,Grades.grade as grade, Grades.short as grd , Posts.Sujet_id , Sujet.nom as sujet FROM Posts, Users, Grades, Sujet WHERE Posts.Users_id = Users.id AND Users.Grades_id =  Grades.id AND Posts.Sujet_id = Sujet.id AND Posts.Casernes_id="+req.query.Casernes_id+" ORDER BY Posts.id";

		connection.query(sql, function(err, rows, fields) {
				if (err){
					console.log(err)
				}
			  
				for(row in rows){

					var post = {};
					post.id = rows[row].id;
					post.horaire_debut = rows[row].horaire_debut;;
					post.horaire_fin = rows[row].horaire_fin;
					post.Sujet_id = rows[row].Sujet_id;
					post.Sujet = rows[row].sujet; 
					post.Users_id = rows[row].Users_id;
					post.User = {}; 
					post.User.nom = rows[row].nom;
					post.User.prenom = rows[row].prenom;
					post.User.Grades_id = rows[row].Grades_id;
					post.User.Grades = rows[row].gradeNom;
					post.User.grade = rows[row].grade;
					post.User.grd = rows[row].grd;
					data.push(post);
					

				};
				next(data)
			});	  
	},					
}

module.exports = Post;
